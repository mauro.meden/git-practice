# Git group practice

Make changes to this repository following the instructor's instructions.

---

Some possible changes:

Add your own section to `index.html`
```html
<section class="student-info">
    <p class="student-name">My name is ...</p>
    <p class="student-likes">I like ...</p>
</section>
```

Add or edit `index.css`. For example
```css
. {
    margin: o;
    padding: 0;
}

body {
    background-color: rgba(41, 40, 145, 0.1);
}
```
