1. Create a branch `git checkout -b name-of-branch`
2. Make some changes
3. `git add .`
4. `git commit -m "Describe the changes"`
5. `git push` (if pushing changes to a branch that already exists remotely) or `git push --set-upstream origin name-of-branch` (if pushing a new branch)
6. Create a merge request in GitLab
7. Go back to main branch `git checkout main`
8. `git pull`

Want to check out a file from another local branch? Try `git checkout branch-name file-name.ext` For example, to check out the file `todo.md` from branch `branch-a`, I would use `git checkout branch-a todo.md`.
Checking out from a remote branch? Try `git checkout origin/check-this-out -- file-to-check-out.txt` (copy this exact code to your command line and then check the file that you checked out). Seeing an error instead? Try `git fetch --all` before running the command above.